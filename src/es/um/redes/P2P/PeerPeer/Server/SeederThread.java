package es.um.redes.P2P.PeerPeer.Server;

import java.io.*;
import java.net.Socket;

import es.um.redes.P2P.PeerPeer.Client.DownloaderThread;
import es.um.redes.P2P.PeerPeer.Message.Message;
import es.um.redes.P2P.PeerTracker.Client.Reporter;
import es.um.redes.P2P.util.FileInfo;

/**
 * Hilo que se ejecuta cada vez que se conecta un nuevo cliente.
 */
public class SeederThread extends Thread {
	private Socket socket = null;

	public SeederThread(Socket socket) {
		super("SeederThread");
		this.socket = socket;
	}

	/**
	 * Función de los hilos que atienden a los clientes.
	 * 
	 * @see java.lang.Thread#run()
	 */

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public void run() {

		Message request = new Message();
		RandomAccessFile rfi = null;
		do {
			try {
				// Recibo de datos

				InputStream is = socket.getInputStream();
				OutputStream os = socket.getOutputStream();
				
				request.fromInputStream(is);
				if ((request.getType() == Message.CONTROL && request.getControlMessage().compareTo("end") == 0)) break;
				if (rfi == null) {
					String filename = new String();
					for (FileInfo file : Reporter.myfiles) {
						// System.out.println(file.fileName + ":" + file.fileHash);
						if (file.fileHash.compareTo(request.getHash()) == 0) {
							filename = file.fileName;
						}
					}
					rfi = new RandomAccessFile(Reporter.sharedFolderPath + "/" + filename, "r");
				}
				
				byte[] chunk;
				if (request.getPart() * DownloaderThread.CHUNK_SIZE + DownloaderThread.CHUNK_SIZE >= rfi.length())
					chunk = new byte[(int) (rfi.length() - request.getPart() * DownloaderThread.CHUNK_SIZE)];
				else
					chunk = new byte[DownloaderThread.CHUNK_SIZE];
				// request.fromInputStream(is);
				//System.out.println("Requested part: " + request.getPart());
				rfi.seek(request.getPart() * DownloaderThread.CHUNK_SIZE);
				rfi.read(chunk);
				//System.out.println("Chunk to send: " + bytesToHex(chunk));
				request.setData(chunk);
				request.setType(Message.SETPART);
				os.write(request.toByteArray());

			} catch (Exception e) {
				break;
			}
		} while (!(request.getType() == Message.CONTROL && request.getControlMessage().compareTo("end") == 0));
		try {
			socket.close();
		} catch (IOException e) {
			;
		}
		if (rfi != null)
			try {
				rfi.close();
			} catch (IOException e) {
				;
			}
	}

}