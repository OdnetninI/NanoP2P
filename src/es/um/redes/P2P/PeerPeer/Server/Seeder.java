package es.um.redes.P2P.PeerPeer.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Servidor que se ejecuta en un hilo propio.
 * Creará objetos {@link SeederThread} cada vez que se conecte un cliente.
 */
public class Seeder implements Runnable {

	public int PORT = 8000;
	
    private InetSocketAddress socketAddress;
    private ServerSocket seederSocket = null;

    public Seeder() {
    }
    
    public int getPort() {
    	return PORT;
    }
    
    public InetSocketAddress getSocketAdderess() {
    	return socketAddress;
    }

    /** 
	 * Función del hilo principal del servidor. 	
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
   		try {
   			while (true)
   			{
   				// Espera una conexión del cliente
   				// El accept retorna un nuevo socket para hablar directamente
   				// con el nuevo cliente conectado
   				Socket s = seederSocket.accept();
   				
   				// Inicia el hilo de servicio al cliente recién conectado,
   				// enviándole el estado general del servidor y el socket de 
   				// este cliente
   				new SeederThread(s).start();
   			}
   		} catch (IOException e) {
   		}
	}
    
    /**
     * Inicio del hilo del servidor.
     */
    public void init()
    {
    	boolean listening = false;
    	while (!listening && PORT < 25000) {
			try {
				socketAddress = new InetSocketAddress(PORT);
			    seederSocket = new ServerSocket();
			    seederSocket.bind(socketAddress);
			    listening = true;
			} catch (IOException e) {
			    PORT++;
			}
    	}

        // Inicia esta clase como un hilo
    	new Thread(this).start();
    	
    	System.out.println("Seeder running on port " +
    			socketAddress.getPort() + ".");
    }
    
    public void close() {
    	if (seederSocket != null)
			try {
				seederSocket.close();
			} catch (IOException e) {
				
			}
    }
}