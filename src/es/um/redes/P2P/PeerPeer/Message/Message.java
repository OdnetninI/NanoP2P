
package es.um.redes.P2P.PeerPeer.Message;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;


// StringBuffer
public class Message {

	private int type = 0;
	private String hash = new String("");
	private int part = 0;
	private int dataLength = 0;
	private String data = new String("");
	private String message = new String("");
	
	public final static int NONE = 0;
	public final static int CONTROL = 1;
	public final static int GETPART = 2;
	public final static int SETPART = 3;
	
	public Message () {
		
	}
	
	public Message (String hash, int part) {
		this.hash = hash;
		this.part = part;
	}
	
	public void setType (int type) {
		this.type = type;
	}
	
	public void setHash (String hash) {
		this.hash = hash;
	}
	
	public void setPart (int part) {
		this.part = part;
	}
	
	public void setEndMessage () {
		this.message = "end";
	}
	
	public String getControlMessage () {
		return message;
	}
	
	public void setData (byte[] buffer) {
		//System.out.println("Data Inserted: " + buffer);
		this.data = DatatypeConverter.printBase64Binary(buffer);
		//System.out.println("Data Extracted: " + data);
		this.dataLength = data.length();
	}
	
	public int getType () {
		return type;
	}
	
	public int getPart () {
		return part;
	}
	
	public int getDataLength() {
		return dataLength;
	}
	
	public byte[] getData () {
		return DatatypeConverter.parseBase64Binary(data);
	}
	
	public String getHash() {
		return hash;
	}
	
	public byte[] toByteArray() {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<message>");
		if (this.type == GETPART) {
			buffer.append("<type>get</type><hash>"+hash+"</hash><part>"+part+"</part>");
		}
		else if (this.type == SETPART) {
			buffer.append("<type>send</type><hash>"+hash+"</hash><part>"+part+"</part>");
			buffer.append("<dataLength>"+dataLength+"</dataLength><data>"+data+"</data>");
		}
		else if (this.type == CONTROL) {
			buffer.append("<type>control</type><part>" + part + "</part><control>" + message + "</control>");
		}
		buffer.append("</message>");
		
		//System.out.println(buffer);
		
		byte[] array = new byte[buffer.length()];
		for (int i = 0; i < buffer.length(); i++)
			array[i] = (byte)buffer.charAt(i);
		return array;
	}
	
	private final static String regexpr = "<(\\w+?)>(.*?)</\\1>";
	private final static String messageXPR = "<message>(.*?)</message>";
	
	public void fromInputStream (InputStream in) throws IOException {
		Pattern pat = Pattern.compile(messageXPR);
		StringBuffer buffer = new StringBuffer();
		Matcher mat = pat.matcher(buffer.toString());
		while (!mat.matches()) {
			buffer.append((char)in.read());
			mat = pat.matcher(buffer.toString());
		}
		fromByteBuffer(buffer);
	}

	
	public void fromByteArray (byte[] array) {
		StringBuffer buffer = new StringBuffer();
		for (byte x : array)
			buffer.append(x);
		fromByteBuffer(buffer);
	}
	
	public void fromByteBuffer(StringBuffer buffer) {		
		Pattern pat = Pattern.compile(regexpr);
		Matcher mat = pat.matcher(buffer.toString());
		while (mat.find()) {
			//System.out.println("Grupo : " + mat.group(1));
			switch (mat.group(1)) {
				case "type" :
					this.type = (mat.group(2).compareTo("get") == 0)? GETPART : ((mat.group(2).compareTo("send") == 0)? SETPART : NONE);
					break;
				case "hash" :
					this.hash = mat.group(2);
					break;
				case "part":
					this.part = Integer.valueOf(mat.group(2));
					break;
				case "dataLength":
					this.dataLength = Integer.valueOf(mat.group(2));
					break;
				case "data":
					//System.out.println("Data: " + mat.group(2));
					this.data = mat.group(2);
					break;
				case "message":
					mat = pat.matcher(mat.group(2));
					break;
				case "control": 
					this.message = mat.group(2);
					break;
			}
		}
	}
}
