package es.um.redes.P2P.PeerPeer.Client;

import java.io.*;
import java.net.*;
import java.util.Calendar;

import es.um.redes.P2P.PeerPeer.Server.SeederThread;
import es.um.redes.P2P.PeerTracker.Client.Reporter;
import es.um.redes.P2P.util.FileDigest;
import es.um.redes.P2P.util.FileInfo;

public class Downloader {

	private RandomAccessFile outputFile;
	private boolean[] pedingParts;
	private boolean[] downloadedParts;
	private int partsDownloaded = 0;
	private int[] partsPerSeed;
	
	public void download(InetSocketAddress[] peers, FileInfo file) throws NumberFormatException, UnknownHostException, IOException
	{
		outputFile = new RandomAccessFile(Reporter.sharedFolderPath + "/" + file.fileName, "rw");
		int parts = (file.fileSize % DownloaderThread.CHUNK_SIZE != 0) ? ((int)(file.fileSize/DownloaderThread.CHUNK_SIZE)) + 1 : (int)(file.fileSize/DownloaderThread.CHUNK_SIZE); 
		pedingParts = new boolean[parts];
		downloadedParts = new boolean[parts];
		partsPerSeed = new int[peers.length];
		for (int i = 0; i < parts; i++) {
			pedingParts[i] = false;
			downloadedParts[i] = false;
		}
		
		for (int i = 0; i < peers.length; i++) {
			Socket socket = new Socket(peers[i].getAddress(), peers[i].getPort());
			partsPerSeed[i] = 0;
			DownloaderThread dt = new DownloaderThread(this,socket, file.fileHash, i);
			dt.start();
		}
		
		long times = System.currentTimeMillis();
		System.out.println("Downloading: \"" + file.fileName + "\"");
		int old = 0;
		while (!finished()) {
			if (partsDownloaded > old) {
				int percentage = (int)((((double)partsDownloaded)/ ((double)downloadedParts.length))*100);
				System.out.print("[");
				for (int i = 0; i < 50; i++) {
					if (i <= percentage/2) System.out.print("#");
					else System.out.print("-");
				}
				System.out.print("] " + percentage + "%\r");
				old = partsDownloaded;
			}
		}
		System.out.print("[");
		for (int i = 0; i < 50; i++) {
			 System.out.print("#");
		}
		System.out.println("] " + 100 + "%");
		times = System.currentTimeMillis() - times;
		outputFile.close();
		System.out.println("Finished Downloading " + file.fileName);
		System.out.println("Number of parts: " + partsDownloaded);
		System.out.println("Total Time: " + getElapsed(times));
		System.out.println("Average Speed: " + file.fileSize/times + " Bytes per second");
		for (int i = 0; i < peers.length; i++) {
			System.out.println("\tSeed: " + peers[i].getHostName() + ":" + peers[i].getPort() + " " + partsPerSeed[i] + " Chunks");
		}
		partsDownloaded = 0;
	}
	
	private String getElapsed (long milis) {
		int dias = (int) ((((milis/1000)/60)/60)/24);
		milis = milis - dias*24*60*60*1000;
		int horas = (int) (((milis/1000)/60)/60);
		milis = milis - horas*60*60*1000;
		int minutos = (int) ((milis/1000)/60);
		milis = milis - minutos*60*1000;
		int segundos = (int) (milis/1000);
		milis = milis - segundos*1000;
		String s = dias + "d " + horas + "h " + minutos + "m " + segundos + "s " + milis + "ms";
		return s;
	}
	
	synchronized private boolean finished () {
		for (int i = 0; i < downloadedParts.length; i++) {
			if (downloadedParts[i] == false) return false;
		}
		return true;
	}
	
	synchronized public int getNewPart (int k) {
		for (int i = 0; i < pedingParts.length; i++) {
			if (!pedingParts[i] && !downloadedParts[i]) { 
				pedingParts[i] = true;
				partsPerSeed[k]++;
				return i;
			}
		}
		return -1;
	}
	
	synchronized public void writeIntoFile (int part, byte[] data) throws IOException {
		//System.out.println("Parte: " + (part+1) + "/"+ downloadedParts.length + " Data: [" + SeederThread.bytesToHex(data) + "]");
		
		if (data.length > DownloaderThread.CHUNK_SIZE) {
			System.out.println("ERROR PART SIZE EXEECED.");
			return;
		}
		if (part > downloadedParts.length) {
			System.out.println("ERROR PART NOT FOUND.");
			return;
		}
		downloadedParts[part] = true;
		partsDownloaded++;
		outputFile.seek(part*DownloaderThread.CHUNK_SIZE);
		outputFile.write(data);
	}
}
