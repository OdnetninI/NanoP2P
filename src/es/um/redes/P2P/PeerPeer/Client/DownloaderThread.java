package es.um.redes.P2P.PeerPeer.Client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import es.um.redes.P2P.PeerPeer.Message.Message;

public class DownloaderThread extends Thread {
	private Socket socket = null;
	private Downloader downloader = null;
	private String fileHash;
	public static final int CHUNK_SIZE = 4096;
	private int seedNum = 0;

	public DownloaderThread(Downloader downloader, Socket socket, String fileHash, int seed) {
		super("DowloaderThread");
		this.socket = socket;
		this.downloader = downloader;
		this.fileHash = fileHash;
		seedNum = seed;
	}

	/** Función de los hilos que atienden a los clientes.
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		int part = 0;
		// En un Socket, para enviar hay que usar su OutputStream		
		do {		
			try {			
				OutputStream os = socket.getOutputStream();
				InputStream is = socket.getInputStream();
				
				part = this.downloader.getNewPart(seedNum);
				if (part == -1) {
					try {
						Message mensaje = new Message();
						mensaje.setType(Message.CONTROL);
						mensaje.setPart(0);
						mensaje.setEndMessage();
						os.write(mensaje.toByteArray());
					} catch (IOException e) {
						;
					}
					break;
				}
				
				Message mensaje = new Message();
				Message response = new Message();
				mensaje.setType(Message.GETPART);
				mensaje.setHash(this.fileHash);

				mensaje.setPart(part);
				//System.out.println("Requested Part: " + part);
				os.write(mensaje.toByteArray());
						
				response.fromInputStream(is);
				this.downloader.writeIntoFile(part, response.getData());
				
			} catch (Exception e) {
				;
			}
			
		} while (part != -1);
		try {
			socket.close();
		} catch (IOException e) {
			;
		}
	}
}
