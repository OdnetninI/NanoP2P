package es.um.redes.P2P.App;

import java.util.Scanner;

import es.um.redes.P2P.PeerTracker.Client.Reporter;

public class SmallSh {
	
	private static final String DEFAULT_PROMPT = "Peer~$ ";
	private static final String SUPER_USER_PROMPT = "Peer~# ";
	private static final String DEBUG_PROMPT = "Peer~? ";
	
	private static final String[] comandosUser = {"ADD", "QUERY", "DOWNLOAD", "EXIT"};
	private static final String[] comandosRoot = {};
	private static final String[] comandosDebug = {};
	
	private int mode = 1;
	private Reporter client = null;
	private boolean executing = true;
	private Scanner scanner = new Scanner(System.in);
	
	public void init (String peerSharedFolder, String trackerHostname) {
		client = new Reporter(peerSharedFolder, trackerHostname, this); 
		String prompt = DEFAULT_PROMPT;
		
		do {
			if (mode == 0) prompt = DEFAULT_PROMPT;
			if (mode == 1) prompt = SUPER_USER_PROMPT;
			if (mode == 2) prompt = DEBUG_PROMPT;
			printOptions();
			System.out.print(prompt);
			readOption();
		}while (executing);
		
		scanner.close();
	}
	
	private void execute (String i, String arg) {
		switch (i) {
			case "ADD": client.sendAddSeed(); break;
			case "DOWNLOAD": client.sendDownload(arg); break;
			case "EXIT": client.sendExit(); terminate(); client.close(); break;
			case "QUERY": client.sendQuery(); break; 
		}
	}
	
	private void printOptions () {
		
		for (int i = 0; i < comandosUser.length; i++)
			System.out.println(i + ".- " + comandosUser[i]);
		
	}
	
	public void changeMode (int mode) {this.mode = mode;}
	public void terminate () {executing = false;}
	
	private void readOption () {
		String line = new String("");
		line = scanner.nextLine();
		String[] lines = line.split(" ");
		String arg = null;
		if (lines.length == 2) arg = lines[1];
		
		boolean executed = false;
		for (String i : comandosUser)
			if (i.compareTo(lines[0]) == 0) {
				executed = true;
				execute(i,arg);
			}
		
		if (mode == 0 && !executed) System.out.println("Command not found.");
		else if (mode == 1) {
			for (String i : comandosRoot)
				if (i.compareTo(lines[0]) == 0) {
					executed = true;
					execute(i, arg);
				}
			if (!executed) System.out.println("Command not found.");
			else if (mode == 2) {
				for (String i : comandosDebug)
					if (i.compareTo(lines[0]) == 0) {
						executed = true;
						execute(i,arg);
					}
				if (!executed) System.out.println("Command not found.");
			}
		}
	}
	
}
