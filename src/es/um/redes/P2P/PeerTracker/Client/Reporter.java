package es.um.redes.P2P.PeerTracker.Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Scanner;

import es.um.redes.P2P.App.SmallSh;
import es.um.redes.P2P.App.Tracker;
import es.um.redes.P2P.PeerPeer.Client.Downloader;
import es.um.redes.P2P.PeerPeer.Server.Seeder;
import es.um.redes.P2P.PeerTracker.Message.Message;
import es.um.redes.P2P.PeerTracker.Message.MessageControl;
import es.um.redes.P2P.PeerTracker.Message.MessageDataFileInfo;
import es.um.redes.P2P.PeerTracker.Message.MessageDataSeedInfo;
import es.um.redes.P2P.util.FileInfo;

public class Reporter {
	/**
	 * Path to local directory whose content is shared in network
	 */
	public static String sharedFolderPath;
	/**
	 * Tracker hostname, for establishing connection
	 */
	private String trackerHostname;		
	
	/**
	 * 
	 */
	private DatagramSocket trackerSocket;
	private boolean connectedToTracker = false;
	
	private Seeder seeder = null;
	private Downloader downloader = new Downloader();
	
	/**
	 * Constructor: 
	 * @param sharedFolder Path to the shared folder of this peer, relative to $HOME
	 */
	public Reporter(String sharedFolder, String tracker, SmallSh smallsh) {
		sharedFolderPath = new String(System.getenv("HOME")+"/"+sharedFolder);
		trackerHostname = tracker;
		myfiles = FileInfo.loadFilesFromFolder(sharedFolderPath);
	}
	
	public Message sendToTracker (Message message) {
		int i = 0;
		while (!connectedToTracker && i < 255) {
			System.out.println("You aren't connected to the tracker.\nRetrying...");
			connectToTracker();
			i++;
			if (i >= 30) {
				System.out.println("Cannot connect to the tracker.");
				return null;
			}
		}
		// Send request
		byte[] buf = message.toByteArray();
		try {
			trackerSocket.send(new DatagramPacket(buf, buf.length));
		} catch (IOException e) {
			System.out.println("Cannot send the packet.");
			return null;
		}
		
		// Receive response
		byte[] buf2 = new byte[Message.MAX_UDP_PACKET_LENGTH];
		DatagramPacket pack = new DatagramPacket(buf2, buf2.length);
		try {
			trackerSocket.receive(pack);
		} catch (IOException e) {
			System.out.println("Cannot receive response the packet.");
			return null;
		}
		
		// Return
		return Message.parseResponse(pack.getData());
	}
	
	private void connectToTracker () {
		try {
			trackerSocket = new DatagramSocket();
			trackerSocket.connect(new InetSocketAddress(trackerHostname, Tracker.TRACKER_PORT));
			connectedToTracker = true;
		} 
		catch (SocketException e) {
			System.out.println("Cannot connect to the tracker.\nDo you want to continue? Y/N");
			@SuppressWarnings("resource")
			Scanner keyboard = new Scanner(System.in);
			String x = "";
			boolean run = true;
			while (run) {
				x = keyboard.nextLine();
				switch (x) {
					case "N": System.exit(1);
					case "Y": run = false; break;
					default: System.out.println("Unrecognized."); break;
				}
			}
		}
	}
	
	private FileInfo[] files = null;
	
	public void sendQuery() {
		MessageDataFileInfo response = (MessageDataFileInfo) sendToTracker(new MessageControl(Message.OP_QUERY_FILES));
		if (response == null || response.getOpCode() != Message.OP_FILE_LIST) {
			System.out.println("Invalid response Message");
			return;
		}
		
		files = response.getFileList();	
		LinkedList<FileInfo> files2 = new LinkedList<FileInfo>();
		
		for (FileInfo file: files) {
			boolean find = false;
			for (FileInfo file2: myfiles) {
				//System.out.println("File: " + file.fileHash + " My: " + file2.fileHash);
				if (file.fileHash.compareTo(file2.fileHash) == 0) {
					find = true; 
					break;
				}
			}
			if (!find) {
				files2.add(file);
			}
		}
		
		for (FileInfo file: files2) {
			System.out.println(file);
		}
		//System.out.println(response);
	}
	
	public static FileInfo[] myfiles = null;
	
	public void sendAddSeed() {
		
		if (seeder == null) {
			seeder = new Seeder();
			seeder.init();
		}

		MessageDataFileInfo request = new MessageDataFileInfo(Message.OP_ADD_SEED, seeder.getPort(),myfiles);		
		
		MessageControl response = (MessageControl) sendToTracker(request);
		if (response == null || response.getOpCode() != Message.OP_ADD_SEED_ACK) {
			System.out.println("Invalid response Message");
			return;
		}
		
		System.out.println("Succesfully added.");
	}
	
	public void sendDownload(String hash) {
		
		FileInfo filea = null;
		if (files != null) {
			for (FileInfo file : files) {
				if (file.fileHash.compareTo(hash) == 0) {
					filea = file;
					break;
				}
			}
		}
		if (filea == null) {
			System.out.println("You can't download a file with hash " + hash + ".\n"+
					"File doesn't exists.");
			return;
		}
		
		MessageDataSeedInfo request = new MessageDataSeedInfo(Message.OP_GET_SEEDS, new InetSocketAddress[0],hash);
		MessageDataSeedInfo response = (MessageDataSeedInfo) sendToTracker(request);
		if (response == null || response.getOpCode() != Message.OP_SEED_LIST) {
			System.out.println("Invalid response Message");
			return;
		}
		
		try {
			downloader.download(response.getSeedList(), filea);
		} catch (Exception e) {
			System.out.println("Error launching downloader.");
		}
		
		myfiles = FileInfo.loadFilesFromFolder(sharedFolderPath);
		sendAddSeed();
	}
	
	public void sendExit() {
		if (seeder != null) {
			Message response = sendToTracker(new MessageDataFileInfo(Message.OP_REMOVE_SEED, seeder.getPort(),myfiles));
			if (response == null || response.getOpCode() != Message.OP_REMOVE_SEED_ACK) {
				System.out.println("Invalid response Message");
				return;
			}
		}
		System.out.println("Terminating...");
	}
	
	public void close () {
		if (trackerSocket.isConnected()) trackerSocket.close();
		if (seeder != null)seeder.close();
		System.out.println("Closed");
	}
	
}
